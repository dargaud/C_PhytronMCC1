#include <stdlib.h>
#include <stdio.h>
#include <iso646.h>
#include <string.h>
#include <unistd.h>	// sleep()
       
#include "TB_Phytron.h"
#include "UseColors.h"

#define SYNTAX printf(BLD WHT "%s [pos1] [pos2] [...]\n"\
						"\tposN\t\tMove Phytron motor to position pos (in deg), then pause\n"\
						NRM, argv[0]);

int main(int argc, const char* argv[]) {
	int Ret, i;
	if (argc==1 or
		argc>1 and 0==strcmp(argv[1], "-h")) { SYNTAX; return 1; }
	
	if (( Ret=TB_Phytron_Init() )) return Ret;

	for (i=1; i<argc; i++) {
		printf("Moving to argv[%d]: %.2f\n", i, atof(argv[i]));
		if (( Ret=TB_Phytron_MoveAbs(atof(argv[i])) )) break; 
		else printf("Press Enter: "), getchar();
	}

	if (Ret) printf(BLD RED "Sequence aborted at step %d: %s\n" NRM, i, TB_Phytron_LastErrMsg());

	printf(BLD WHT "%s\n" NRM, TB_Phytron_Status());

	TB_Phytron_Close();
	return Ret;
}
