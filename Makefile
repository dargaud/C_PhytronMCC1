phytron = TB_Phytron

REMOTE?=0	# This is when running test progs from remote system via ssh tunnels. Also use "make REMOTE=1"
COPT = -ggdb -O0 -Wall -std=gnu11 -fdiagnostics-color=always -DUSE_COLOR_MSG -DREMOTE=${REMOTE}
CC=gcc
TP=-DSTANDALONE_TEST_PROG

$(phytron): $(phytron).c $(phytron)_QuickTest.c
	@# $(CC) $(COPT)                      -c $<		# Compile as obj to test lib capabilities (optional)
	$(CC) $(COPT)                         $< -o $@ $(TP)	# Compile as standalone test program
	$(CC) $(COPT) -Wno-unused-function    $^ -o $@_QuickTest

clean:
	rm -f $(phytron) *_QuickTest *_Calib core* *.o
