// NOTE: Command protocol inspired from API found at https://lpsc.in2p3.fr/svn/akido/NIKA/software/mcc1-client/

// NOTE: hardware references:
// Model of controller:	Phytron MCC-1
// Model of Motor:		ZSS 25.200.1,2
// Model of encoder:	HEDL-5540-A14
// Model of gearing:	GPL026-2 46:1 or I-GPL26-4Lp

// NOTE: about reverse-engineering the way MiniLog-comm works:
//	See https://wiki.wireshark.org/CaptureSetup/USB
//	connect a USB-Ethernet to Linux PC, pass the USB device to Windows VM, run Minilog-Comm in VM
//	Make sure beforehand that the VM IP is a static 192.168.0.2 on that USB-Eth adapter
//	[Options][Interface Parameters][Eth-MCCe][Static IP]
//	Connect to IP: 192.168.0.31 / Controller's MAC: 11 75 / Contoller's IP 192.168.0.31
//	Press [Connect], wait and cross your fingers
// Then once the comm is up, in Linux, sudo modprobe usbmon, then launch sudo wireshark (even if you have set the groups right, it won't work for USB without sudo)
//	lsusb on ASIX will give you the bus N. Launch wireshark or tcpdump capture on usbmonN
//	Wireshark: Filter for specific device (you have to guess): usb.dst == "3.9.3" or usb.src == "3.9.2"
//	There should be incoming packets containing things like ACK or outgoing like 0SB
//	To grep the outgoing commands:
// sudo tcpdump -i usbmon3 -X -c 1000 | awk '{ if (match($0, /^[0-9]/, _)) { printf (NR == 1 ? "%s " : "\n%s "), $0; fflush() } else { sub(/^\s+0x[0-9a-z]+:\s+/, " "); gsub(" ", ""); printf "%s", $0 } } END { print ""; fflush() }' | grep " to 3:9:" | sed -e "s/.*\.\.\.\.//" -e "s/\..*//" | sort | uniq -c | sort -n
// or better, filter it through my own TcpDumpJoin (on gitlab)

// WARNING: the address is 0, the axis is 1 (X), don't confuse the two.

// NOTE: Telegram format:
// Sending: <STX> Address Axis ... <ETX>
// Reply:   <STX> <ACK> <ETX>, <STX> <NACK> <ETX> or <STX> <ACK> Reply <ETX>, 

#include <iso646.h>		// and, or...
#include <stdlib.h>		// exit
#include <stdio.h>		// printf
#include <string.h>		// strlen
#include <stdarg.h>		// va_arg
#include <errno.h>
#include <math.h>		// Needs -lm
#include <unistd.h>		// close, usleep
#include <signal.h> 	// Ctrl-C trap
#include <sys/socket.h>	// socket
#include <arpa/inet.h>	// inet_addr

#include "UseColors.h"
#include "TB_Phytron.h"

tPhytron Phytron={
	.IP="192.168.0.31",		// This is defined by the dhcp server configuration. See /etc/dhcp/dhcpd.conf file
	.Port=22222,
	.msWait=10,				// Min delay between command and reply and new command. 50ms seems safe enough
	.msMotionPolling=500,	// Same as above, but higher value to limit the number of polling messages
	.SpeedDiv=10,			// Range: 1 to 256. Default is 6 or 10. 1 is too fast and might break the switch
	.MoveInDiode_Pos=34,	// Precise angle where the diode is in the middle of the beam
	.OpMin=0, .OpMax=70,	// Operational motion limits (in deg)
	.MainDir=+1,			// +1 if the homing should go for the positive direction, -1 otherwise.
							// Don't get this wrong or you will BREAK the support
	.UseEncoder=0
};

// So that the various instruments have different alignments to make their logging more readable when intermixed
static char Tabs[]="\t\t";
#define MAX_REPLY 1024

#define STX  0x02	// Start of Text
#define ETX  0x03	// End of Text
#define ACK  0x06
#define NACK 0x15
#define ID   '0'	// Device address

// For X command, see PH_AxisStatusRequest()
enum TcheckAxisStatus {	powStageErr,      powStageNormal, 
						axisOnStandstill, axisInMotion,
						axisAtMax,        axisAtMin, 
						checkStepFailure, checkStepSuccess,
						emergencyStop,    normalStop };

enum { D_ALL, D_NO_REPLY, D_NONE };	// Display cmd&reply, just cmd or neither
static int Display=D_ALL;			// Global to skip displaying some commands or replies

static int sock=0;	// Socket

static int Enabled=0;
static char LastErrMsg[1024]="";
#define PrintErr(f_, ...) sprintf(LastErrMsg, (f_), ##__VA_ARGS__), printf(BLD RED "%s%s\n" NRM, Tabs, LastErrMsg)
#define msleep(ms) usleep((ms)*1000)// ms sleep

static void (*PreviousSignalHandlerPipe)(int) = NULL;
static void (*PreviousSignalHandlerInt )(int) = NULL;

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Trap handler to disable drives and close sockets properly
////////////////////////////////////////////////////////////////////////////////
static void intHandler(int Signal) {
	if (Signal==SIGPIPE) puts(BLD RED "\n*** Broken pipe - Closing Phytron ***\n" NRM);
	if (Signal==SIGINT ) puts(BLD RED "\n*** Abort - Closing Phytron ***\n" NRM);
	// PH_CloseAll();
	TB_Phytron_Close();
	if (Signal==SIGPIPE and PreviousSignalHandlerPipe) PreviousSignalHandlerPipe(Signal);
	if (Signal==SIGINT  and PreviousSignalHandlerInt)  PreviousSignalHandlerInt (Signal);
	exit(2);
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Initializes socket communication
////////////////////////////////////////////////////////////////////////////////
int PH_Init(void) {
	struct sockaddr_in server;
	
	// Create socket
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1) {
		PrintErr("Could not create socket: %s", strerror(errno));
		return errno;
	}
	printf("%sSocket created for Phytron\n", Tabs);

	// Prepare the sockaddr_in structure
	server.sin_addr.s_addr = inet_addr(Phytron.IP);
	server.sin_family = AF_INET;
	server.sin_port = htons( Phytron.Port );

	// Connect to remote server
	if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) 	{
		PrintErr("Connect to %s:%d failed: %s", Phytron.IP, Phytron.Port, strerror(errno));
		return errno;
	}
	
	PreviousSignalHandlerPipe=signal(SIGPIPE, intHandler);	// Note: when used as a lib we can't have multiple of those. See https://stackoverflow.com/questions/17102919/is-it-valid-to-have-multiple-signal-handlers-for-same-signal
	PreviousSignalHandlerInt =signal(SIGINT,  intHandler);   // Ctrl-C
	//	printf("Previous handlers: pipe=%p, int=%p\n", PreviousSignalHandlerPipe, PreviousSignalHandlerInt);
	
	printf(BLD YEL "%sPhytron connected\n" NRM, Tabs);
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Close all socket communications
////////////////////////////////////////////////////////////////////////////////
int PH_CloseAll(void) {
	if (sock) {
		PH_DoAxisStop();
		PH_EnAxisPowerStage(0);
		close(sock); sock=0;
	}
	fflush(NULL);
	printf("Bye\n");
    return 0;
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN    Send a command to a device and read the answer
/// HIRET	0 if no problem, errno otherwise
////////////////////////////////////////////////////////////////////////////////
int PH_SendCommand(char* Reply, const char* Command, ... ) {
	int N, L=strlen(Command);
	char Cmd[L+10], Cmd2[L+20];
	va_list str_args;

	va_start( str_args, Command );
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Wformat"
	/*int Nb=*/ vsnprintf(Cmd, 256, Command, str_args);
//#pragma clang diagnostic pop
	va_end( str_args );
		
	// Add required control chars
	sprintf(Cmd2, "%c%c%s%c", STX, ID, Cmd, ETX);
	
	Reply[0]='\0';
	if (send(sock, Cmd2, strlen(Cmd2), 0) < 0) {
		PrintErr("Send failed: %s", strerror(errno));
		return errno;
	}
	if (Display!=D_NONE) 
		printf("Sent :%s%s\n", Tabs, Cmd);
	msleep(Phytron.msWait);

	// Receive a reply from the server
//    if (recv(sock, Reply, MAX_REPLY, 0) < 0) {
	if ((N=read(sock, Reply, MAX_REPLY)) < 0) {
		PrintErr("Recv failed: %s", strerror(errno));
		return errno;
	}
	Reply[N]='\0';

	// Clean up reply STX/ACK/ETX if in correct format
	// NOTE: sometimes there seems to be extra chars (0) after ETX, but with C strings it doesn't matter
	if (strlen(Reply)>3 and Reply[0]==STX and Reply[1]==ACK and Reply[strlen(Reply)-1]==ETX) {
		memmove(Reply, Reply+2, strlen(Reply)-2+1);	// Includes the final \0
		Reply[strlen(Reply)-1]='\0';
	} else if (strlen(Reply)==3 and Reply[0]==STX and Reply[1]==ACK and Reply[2]==ETX) 
		strcpy(Reply, "-ACK-");
	else if (strlen(Reply)==3 and Reply[0]==STX and Reply[1]==NACK and Reply[2]==ETX) 
		strcpy(Reply, "-NACK-");
	else if (strlen(Reply)==0) 
		strcpy(Reply, "-empty-");
	
	if (Display==D_ALL)
		printf("Reply:%s%s\n", Tabs, Reply);
	msleep(Phytron.msWait);
	return 0;
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return SB status string
/// HIPAR	Reply/As returned by SB
/// HIRET	Message(s)
////////////////////////////////////////////////////////////////////////////////
char* PH_GetStatusBinaryMsg(char *Reply) {
	static char List[1024]=""; List[0]='\0';
	if (Reply==NULL or Reply[0]=='\0' or strlen(Reply)!=8) return "N/A";
	if (Reply[0]=='1')                                    strcat(List, "Program Run");
	if (Reply[1]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "Software Remote"); }
	if (Reply[2]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "Emergency limit switch of an axis"); }
	if (Reply[3]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "Power stage failure of an axis"); }
	if (Reply[4]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "Error programming (reset after status request)"); }
	if (Reply[5]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "Terminal is activated"); }
	if (Reply[6]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "SRQ has been set"); }
	if (Reply[7]=='1') { if (List[0]) strcat(List, ", "); strcat(List, "Computer call"); }
	return List[0] ? List : "Unknown status value";
}

////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return ST status string
/// HIPAR	Status/As converted from ST reply with Status = strtoul(Reply, NULL, 0x10)
/// HIRET	Message(s)
////////////////////////////////////////////////////////////////////////////////
char* PH_GetSystemStatusMsg(int Status) {
	static char List[1024]=""; List[0]='\0';
	if ((Status>>0)&1)                                    strcat(List, "End of program in the LOCAL MODE");
	if ((Status>>1)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Program run"); }
	if ((Status>>2)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Software remote"); }
	if ((Status>>3)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Emergency limit switch on axis"); }
	if ((Status>>4)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Power stage failure of an axis"); }
	if ((Status>>5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Error programming (reset after status request)"); }
	if ((Status>>6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Terminal or Enable is activated"); }
	if ((Status>>7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "SRQ has been set"); }
	if ((Status>>8)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Computer mode"); }
	return List[0] ? List : "Unknown status value";
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Return SE status string
/// HIPAR	Status/As converted from SE reply with Status = strtoul(Reply, NULL, 0x10)
/// HIRET	Message(s)
////////////////////////////////////////////////////////////////////////////////
char* PH_GetSystemStatusExtendedMsg(int Status) {
	static char List[1024]=""; List[0]='\0';
	if ((Status&7)==7) { if (List[0]) strcat(List, ", "); strcat(List, "No power stage is connected"); }
	else {
	if ((Status>>0)&1)                                    strcat(List, "Power stage error");
	if ((Status>>1)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Power stage under voltage"); }
	if ((Status>>2)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Power stage overtemperature"); }
	}
	if ((Status>>3)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Power stage is activated"); }
	if ((Status>>4)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Initiator - is activated (emergency stop)"); }
	if ((Status>>5)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Initiator + is activated"); }
	if ((Status>>6)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Step failure"); }
	if ((Status>>7)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Encoder error"); }
	if ((Status>>8)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Motor stands still"); }
	if ((Status>>9)&1) { if (List[0]) strcat(List, ", "); strcat(List, "Reference point is driven and OK (is reset at top by initiator)"); }
	return List[0] ? List : "Unknown status value";
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Any axis running ?
/// HIRET	1 if some axes are running, 0 if all stopped
////////////////////////////////////////////////////////////////////////////////
int PH_GetStatusAxes(void) {
	char Reply[MAX_REPLY];
	PH_SendCommand(Reply, "SH"); 
	printf(BLD YEL "%s%s\n" NRM, Tabs,
		   Reply[0]=='E'?"All axes are stopped":"Some axes are running");
	return Reply[0]=='N';
}


////////////////////////////////////////////////////////////////////////////////
/// HIFN	Initiator status
/// HIRET	0/-1/1/2
////////////////////////////////////////////////////////////////////////////////
int PH_GetInitiatorsStatus(void) {
	char Reply[MAX_REPLY];
	int R;
	PH_SendCommand(Reply, "SUI");
	if (strlen(Reply)!=3 or Reply[0]!='I' or Reply[1]!='=') {
		PrintErr("Wrong reply %.80s", Reply);
		return -3;
	}

	switch (Reply[2]) {
		case '0':R= 0; printf(BLD YEL "%sAxis is free, no initiator has reacted\n" NRM, Tabs); break;
		case '+':R= 1; printf(BLD YEL "%sInitiator + direction has reacted\n" NRM, Tabs); break;
		case '-':R=-1; printf(BLD YEL "%sInitiator - direction has reacted\n" NRM, Tabs); break;
		case '2':R= 2; PrintErr("Both initiators have reacted (wrong polarity, broken wire or no 24V supply voltage)"); break;
		default :R=-2; PrintErr("Initiators: Wrong answer %.80s", Reply); break;
	}
	return R;
}

/////////////////////////////////////////////////////////////////////
/// HIFN	Reset the controller
/// HIFN	NOTE: This can take up to 10s and the LED turns red for a while during reset.
/////////////////////////////////////////////////////////////////////
int PH_Reset(void) {
	char Reply[MAX_REPLY];
	int Ret=PH_SendCommand(Reply, "CR");
	printf(BLD PRP "%sRESET (please wait)\n" NRM, Tabs);
	sleep(10);
	return Ret;
}

/////////////////////////////////////////////////////////////////////
/// HIFN	You should also reset the EL0P P[19] after this
/// HIPAR	Dir/-1 to move to - initiator, +1 to move to + initiator
/////////////////////////////////////////////////////////////////////
int PH_MoveToLimit(int Dir) {
	char Reply[MAX_REPLY];
	int Ret=PH_SendCommand(Reply, "X0%c", Dir<0 ? '-' : '+');
	printf(BLD YEL "%sMove to limit %c\n" NRM, Tabs, Dir<0 ? '-' : '+');
	return Ret;
}

/////////////////////////////////////////////////////////////////////
/// HIRET	Number of connected axes
/////////////////////////////////////////////////////////////////////
int PH_GetAxisCount(void) {
	char Reply[MAX_REPLY]="";
	PH_SendCommand(Reply, "S");
	printf(BLD YEL "%sAxis count: %d\n" NRM, Tabs, atoi(Reply));
	return atoi(Reply);
}


/////////////////////////////////////////////////////////////////////
/// HIFN	Do one of 6 type axis of status request
/// HIPAR	val/See TcheckAxisStatus
/// HIRET	0 if no error
/////////////////////////////////////////////////////////////////////
int PH_AxisStatusRequest(int val) {
	char Reply[MAX_REPLY]="";
	switch (val) {	
		case powStageErr:		PH_SendCommand(Reply, "X=E");	break;
		case powStageNormal:	PH_SendCommand(Reply, "X#E");	break;
		case axisOnStandstill:	PH_SendCommand(Reply, "X=H");	break;
		case axisInMotion:		PH_SendCommand(Reply, "X#H");	break;
		case axisAtMax:			PH_SendCommand(Reply, "X=I+");	break;
		case axisAtMin:			PH_SendCommand(Reply, "X=I-");	break;
		case checkStepFailure:	PH_SendCommand(Reply, "X=M");	break;	//device may not be equipped - Needs SFI board
		case checkStepSuccess: 	PH_SendCommand(Reply, "X#M");	break;	//device may not be equipped - Needs SFI board
		case emergencyStop:		PH_SendCommand(Reply, "X=N");	break;
		case normalStop:		PH_SendCommand(Reply, "X#N");	break;
	}

	printf(BLD YEL "%sAXIS status request %d: %s\n" NRM, Tabs, val, Reply);
	return (Reply[0]=='E');
}

/////////////////////////////////////////////////////////////////////
int PH_ResetAxis(void) {
	char Reply[MAX_REPLY]="";
	int Ret=PH_SendCommand(Reply, "XC");
	printf(BLD YEL "%sRESET axis\n" NRM, Tabs);
	return Ret;	
}

/////////////////////////////////////////////////////////////////////
int PH_DoAxisStop(void) {
	char Reply[MAX_REPLY]="";
	int Ret=PH_SendCommand(Reply, "XS");
	printf(BLD YEL "%sSTOP\n" NRM, Tabs);
	return Ret;
}

/////////////////////////////////////////////////////////////////////
int PH_EnAxisPowerStage(int val) {
	char Reply[MAX_REPLY]="";
	int Ret=PH_SendCommand(Reply, "XM%c", val?'A':'D');
	printf(BLD YEL "%sPower stage %s\n" NRM, Tabs, val?"ACTIVATE":"DEACTIVATE");
	return Ret;
}


/////////////////////////////////////////////////////////////////////
#define NbP	50	// Number of param fixed by hardware. See 5.1 of manual
static int    Parameter [NbP]={0};
static double ParameterF[NbP]={0.};	// Same as above, as floats, for some parameters only
static const int ParamIsF[NbP]={ [3]=1, [19]=1 };	// True if the parameter is a float value

static char *ParameterTxt[NbP]={			// PT[n] indicates optional argument list, see PT[][] below
	"not used",
	"Type of movement",						// PT[1]
	"Measuring units of movement",			// PT[2]
	"Conversion factor for the thread",		// FLOAT
	"Start/stop frequency (Hz)",
	"not used",
	"not used",
	"Emergency stop ramp (in 4000-Hz/sec-steps)",
	"fmax M0P run frequency during initializing (referencing) (Hz).",
	"Ramp M0P (in 4000-Hz/sec-steps)",
// 10
	"fmin M0P Run frequency for leaving the limit switch range (Hz)",
	"M0P offset for limit switch direction +",
	"M0P offset for limit switch direction –",
	"Recovery time M0P (msec)",
	"fmax Run frequency during program operation (Hz)",
	"Ramp for run frequency (P14) (in 4000-Hz/sec-steps (4000 to 500000 Hz/sec))",
	"Recovery time position (ms)",
	"Boost (defined in P42)",				// PT[17]
	"not used",
	"Electronical zero counter (for setting operating points)",	// FLOAT
// 20
	"Mechanical zero counter (number of steps referred to the M0P)",
	"Absolute counter",
	"Encoder counter",						// Indicates the true encoder position.
	"Axial limitation pos. direction + (0=no limitation)",	// If the number of steps is reached, the run in + direction is aborted. 0 = no limitation
	"Axial limitation neg. direction –",
	"Compensation for play (steps)",		// Indicates the step number, the target position in the selected direction is passed over and afterwards is started in reverse direction.
	"not used",
	"Initiator type",						// PT[27]
	"not used",
	"not used",
// 30
	"not used",
	"not used",
	"not used",
	"not used",
	"Encoder type",							// PT[34]
	"Encoder resolution for SSI encoder (in bits, max 31bits)",
	"Encoder function (0=counter)",
	"not used",
	"Encoder preferential direction of rotation (0=+, 1=–)",
	"Encoder conversion factor",			// 1 increment corresponds to ...
// 40
	"Stop current (in 0.1A)",
	"Run current (in 0.1A)",
	"Boost current (in 0.1A)",
	"Current delay time (msec)",
	"not used",
	"Step resolution (1 to 256)",			// 1=Full step, 2=Half step, 4=1/4 step (default), 256=max resolution... Not sure if all values are possible
	"Current Shaping (CS), also see appendix A",	// PT[46]
	"Chopper frequency",					// PT[47]
	"Power stage type (read only)",			// PT[48]
	"Power stage temperature (degC)"			// (only for linear power stage type)
};

static char *PT[NbP][5]={	[1]={"rotational", "linear"},
						[2]={"", "step", "mm", "inch", "degree"},
						[17]={"off", "on during motor run", "on during acceleration and deceleration ramp"},
						[27]={"PNP normally closed contact (NCC)", "PNP normally open contact (NOC)"},
						[34]={"none", "incremental", "serial interface SSI binary Code", "serial interface SSI Gray Code"},
						[46]={"Off", "On"},
						[47]={"low", "high"},
						[48]={"linear", "chopper"}
};

/////////////////////////////////////////////////////////////////////
/// HIRET	0 if no error
/////////////////////////////////////////////////////////////////////
int PH_SetParamValue(int Param, int Val) {
	char Reply[MAX_REPLY], Extra[256]="";
	if (Param<0 or NbP<=Param or
		Param==3 or Param==19) { PrintErr("Invalid parameter %d", Param); return -1; }
	int Ret=PH_SendCommand(Reply, "XP%dS%d", Param, Parameter[Param]=Val);
	if (Val>=0 and Val<5 and PT[Param][Val]!=NULL and PT[Param][Val][0]!='\0')
		sprintf(Extra, ": %s", PT[Param][Val]);
	printf(BLD YEL "%sSetting param %d to %d (%s%s)\n" NRM, Tabs, Param, Val, ParameterTxt[Param], Extra);
	return Ret;
}

/////////////////////////////////////////////////////////////////////
/// HIPAR	Param/Only valid for 3 and 19
/// HIRET	0 if no error
/////////////////////////////////////////////////////////////////////
int PH_SetParamValueF(int Param, double Val) {
	char Reply[MAX_REPLY];
	if (!(Param==3 or Param==19)) { PrintErr("Invalid parameter %d", Param); return -1; }
	double Ret=PH_SendCommand(Reply, "XP%dS%.2f", Param, ParameterF[Param]=Val);
	printf(BLD YEL "%sSetting param %d to %.2f (%s)\n" NRM, Tabs, Param, Val, ParameterTxt[Param]);
	return Ret;
}

/////////////////////////////////////////////////////////////////////
/// HIRET	Integer parameter value is read and returned
/////////////////////////////////////////////////////////////////////
int PH_GetParamValue(int Param) {
	char Reply[MAX_REPLY]="", Extra[256]="";
	if (Param<0 or NbP<=Param or
		Param==3 or Param==19) { PrintErr("Invalid parameter %d", Param); return -1; }
	PH_SendCommand(Reply, "XP%dR", Param);
	int Val=Parameter[Param]=atoi(Reply);
	if (Val>=0 and Val<5 and PT[Param][Val]!=NULL and PT[Param][Val][0]!='\0')
		sprintf(Extra, ": %s", PT[Param][Val]);
	printf(BLD YEL "%sParam %d: %s (%s%s)\n" NRM, Tabs, Param, Reply, ParameterTxt[Param], Extra);
	return Val;
}

/////////////////////////////////////////////////////////////////////
/// HIFN	Some values are floats.
/// HIPAR	Param/Only valid for 3 and 19
/// HIRET	Parameter value is returned
/////////////////////////////////////////////////////////////////////
double PH_GetParamValueF(int Param) {
	char Reply[MAX_REPLY]="";
	if (!(Param==3 or Param==19)) { PrintErr("Invalid parameter %d", Param); return -1; }
	PH_SendCommand(Reply, "XP%dR", Param);
	double Val=ParameterF[Param]=atof(Reply);
	printf(BLD YEL "%sParam %d: %s (%s)\n" NRM, Tabs, Param, Reply, ParameterTxt[Param]);
	return Val;
}


/////////////////////////////////////////////////////////////////////
static int GearReduction=26;	// ??? 46;	// See GPL26 manual
#define StepRes (Parameter[45])						// Must be read with PH_GetParamValue prior to using those macros
#define P2D(p) ((double)(p)/(GearReduction*StepRes))// From position (steps) to degrees
#define D2P(d) ((d)*GearReduction*StepRes)			// From degrees to position (steps)

/////////////////////////////////////////////////////////////////////
/// HIFN	Move to relative position
/// HIPAR	val/Position in degrees
/////////////////////////////////////////////////////////////////////
int PH_RelPosition(double val) {
	char Reply[MAX_REPLY]="";
	int Ret=PH_SendCommand(Reply, "X%.2f", D2P(val));
	printf(BLD YEL "%sRelPos: %.1f\n" NRM, Tabs, val);
	return Ret;
}

/////////////////////////////////////////////////////////////////////
/// HIFN	Move to absolute position
/// HIPAR	val/Position in degrees
/////////////////////////////////////////////////////////////////////
int PH_AbsPosition(double val) {
	char Reply[MAX_REPLY]="";
	int Ret=PH_SendCommand(Reply, "XA%.2f", D2P(val));
	printf(BLD YEL "%sAbsPos: %.1f\n" NRM, Tabs, val);
	return Ret;
}

/////////////////////////////////////////////////////////////////////
int PH_FreeRunning(int val) {
	char Reply[MAX_REPLY]="";
	int Ret=PH_SendCommand(Reply, "XL%c", val<0?'-':'+');
	printf(BLD YEL "%sFree running %c\n" NRM, Tabs, val<0?'-':'+');
	return Ret;
}


///////////////////////////////////////////////////////////////////////////////
/// HIFN	Use 4 different methods to get the current motor position
/// HIRET	Current position in degrees (relative to M0P or EL0P)
/////////////////////////////////////////////////////////////////////
double PH_DbgGetPosition(double *P) {
	// This is a double with 2 digits precision (so 1/100th step in theory), 
	// it's the most precise positioning parameter but must be zeroed at start
	P[19]=PH_GetParamValueF(19);		// Electronical zero counter. Used for setting operating points. 
										// At standstill of the axis, P19 can be read or programmed during program execution.
										// Before you can use this you must reset EL0P with Set(19,0)

	// This has only a precision of 1 step
	P[20]=PH_GetParamValue(20);		// Mechanical zero counter. This counter contains the number of steps referred to the mechanical zero (M0P). Can be read at axis standstill. If the axis reaches the M0P, P20 will be set to zero.

	// This has only a precision of 1 step. Seems to be always the same as P[20]
	P[21]=PH_GetParamValue(21);		// Absolute counter. Encoder, multi turn and also for single turn.

	// Only available if encoder is used, but... WTF is this value ? Seems to be P[19]*333.6
	P[22]=PH_GetParamValue(22);
	
	printf(BLD PRP "Pos:%s%.1fdeg\n" NRM, Tabs, P2D(P[19]));	// 19 works only without encoder. 20 works with both.  21 and 22 always seem at 0 ?
	return P2D(P[19]);
}

///////////////////////////////////////////////////////////////////////////////
/// HIRET	Current position in degrees (relative to EL0P)
///////////////////////////////////////////////////////////////////////////////
double PH_GetPosition(void) {
	// This is a double with 2 digits precision (so 1/100th step in theory), 
	// it's the most precise positioning parameter but must be zeroed at start
	double P=PH_GetParamValueF(19);		// Electronical zero counter. Used for setting operating points. At standstill of the axis, P19 can be read or programmed during program execution.
	printf(BLD PRP "Pos:%s%.1fdeg\n" NRM, Tabs, P2D(P));
	return P2D(P);
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	General init sequence
///////////////////////////////////////////////////////////////////////////////
int PH_DoGetGeneralStatus(void) {
	char Reply[MAX_REPLY]="";
	int Status;
	PH_GetAxisCount();
	
	PH_SendCommand(Reply, "ST"); 
	printf(BLD YEL "%sST: %s\n" NRM, Tabs, PH_GetSystemStatusMsg(Status = strtoul(Reply, NULL, 0x10)));
	
	int val=PH_GetInitiatorsStatus();
	printf(BLD YEL "%sNb initiator(s): %d\n" NRM, Tabs, val);
	
	PH_AxisStatusRequest(powStageErr);
	PH_AxisStatusRequest(powStageNormal);
	PH_AxisStatusRequest(axisOnStandstill);
	PH_AxisStatusRequest(axisInMotion);
	PH_AxisStatusRequest(axisAtMax);
	PH_AxisStatusRequest(axisAtMin);
	//PH_AxisStatusRequest(checkStepFailure);
	//PH_AxisStatusRequest(checkStepSuccess);
	PH_AxisStatusRequest(emergencyStop);
	PH_AxisStatusRequest(normalStop);
	
	// For parameter values, see MA 1240-A008 EN, section 5
 	PH_SetParamValue(27, 0);	// Initiator type, 0 = PNP normally closed contact (NCC)
	PH_SetParamValue(8, 400);	// fmax M0P (mechanical zero point). Run frequency during initializing (referencing) in Hz

	for (int i=0; i<NbP; i++)	// Get all parameters (and store them in an array)
		if (0!=strcmp(ParameterTxt[i], "not used")) {
			if (ParamIsF[i]) PH_GetParamValueF(i); 
			else             PH_GetParamValue(i);
		}

	PH_EnAxisPowerStage(1);

	PH_SendCommand(Reply, "ST"); 
	printf(BLD YEL "%sST: %s\n" NRM, Tabs, PH_GetSystemStatusMsg(Status = strtoul(Reply, NULL, 0x10)));
	if ((Status>>5)&1) PH_ResetAxis();

	PH_SendCommand(Reply, "SE"); 
	printf(BLD YEL "%sSE: %s\n" NRM, Tabs, PH_GetSystemStatusExtendedMsg(Status = strtoul(Reply, NULL, 0x10)));
	if ((Status>>5)&1) PH_ResetAxis();

	PH_GetStatusAxes();
	PH_DoAxisStop();
	return 0;
}

#define MSG "ERROR: No reply from controller, aborting.\n"\
			"\tMake sure you don't have an already opened telnet connection to that device.\n"\
			"\tTo find the guilty process, use: lsof -n | grep 192.168.0.\n"

////////////////////////////////////////////////////////////////////////////////
#ifdef STANDALONE_TEST_PROG

int main(int argc, const char* argv[]) {
	char Reply[MAX_REPLY];
	int Ret=0, i;
	
	if (argc>1 and (0==strcmp("-h", argv[1]) or 0==strcmp("--help", argv[1]))) {
		fprintf(stderr, "%s [Pos|@Cmd] ...\n"
				"Pos:\tAbsolute positions to move the motor to (numerical value, in degrees)\n"
				"@Cmd:\tArbitrary commands to send to the controller (must follow a '@'). No sanity check is performed\n"
				"Currently %susing the encoder\n"
				, argv[0], Phytron.UseEncoder?"NOT ":"");
		return 1;
	}
	
	if (PH_Init()) return errno; 
	Enabled=1;

	Ret|=PH_SendCommand(Reply, "IVR"); printf(BLD YEL "%s%s\n" NRM, Tabs, Reply);
	printf(BLD WHT "%s\n" NRM, TB_Phytron_Status());
	
	Ret|=PH_Reset();
//	Ret|=PH_ResetAxis();		// not necessary
	Ret|=PH_DoGetGeneralStatus();

	Ret|=PH_SetParamValue(45, 4 /*FIXME Phytron.SpeedDiv */);				// 1 for full step. Default was 1/6 ?
		
	Ret|=PH_SetParamValue(2, 1);				// Measuring unit of movement - Step
//	Ret|=PH_SetParamValue(2, 4);				// Measuring unit of movement - Degree - DOES NOT SEEM DO ANYTHING
	Ret|=PH_SetParamValue(39, GearReduction);	// Encoder conversion factor - DOES NOT SEEM TO DO ANYTHING

	double P[argc][NbP];	// Positioning tests, in steps
	double Q[argc][NbP];	// Positioning tests, in steps
	if (Ret) goto Skip;

	#define PAUSE printf("Press Enter: "), getchar()
	
#if 1	// Check the effect of limits/timeout on initiator motion
	Ret|=PH_SetParamValue(23, D2P(90));				// Axis + limitation
	Ret|=PH_SetParamValue(24, D2P(0));				// Axis - limitation
	// It works but it doesn't make sense: it should be the opposite
#endif

	Ret|=PH_MoveToLimit(Phytron.MainDir);		// If no contactor, will stop after a timeout (18s I think. It's one of the parameters)
	do msleep(Phytron.msMotionPolling); while (PH_GetStatusAxes());
//	PH_DbgGetPosition(Q[i]);
	PAUSE;
	Ret|=PH_SetParamValueF(19, 0.);				// Reset EL0P

	
#if 0	// Check the effect of the resolution. It's important at low angles because of truncation
	PH_SetParamValue(34, Phytron.UseEncoder);
	double POS=90.;
	Ret|=PH_SetParamValue(45, 1);				// Full step. Default was 4
	TB_Phytron_MoveAbs(POS);
	printf("StepRes=%d, %.02fdeg=%.02fpos, current=%.02fdeg\n", StepRes, POS, D2P(POS), PH_GetPosition());
	TB_Phytron_MoveAbs(0);
	
	Ret|=PH_SetParamValue(45, 10/*4*/);			// Default
	TB_Phytron_MoveAbs(POS);
	printf("StepRes=%d, %.02fdeg=%.02fpos, current=%.02fdeg\n", StepRes, POS, D2P(POS), PH_GetPosition());
	TB_Phytron_MoveAbs(0);

	Ret|=PH_SetParamValue(45, 100/*256*/);				// Max resolution, very slow
	TB_Phytron_MoveAbs(POS);
	printf("StepRes=%d, %.02fdeg=%.02fpos, current=%.02fdeg\n", StepRes, POS, D2P(POS), PH_GetPosition());
	TB_Phytron_MoveAbs(0);
	exit(1);
#endif
	
	PH_SetParamValue(34, 0);					// no encoder
	for (i=1; i<argc; i++) {
		printf("\nArgv:%s%s\n", Tabs, argv[i]);
		if (argv[i][0]=='@') {	// If @ as first char: arbitrary command
			PH_SendCommand(Reply, "%s", argv[i]+1);
			//msleep(Phytron.msMotionPolling);
		} else {	// Otherwise assume numerical absolute moves
			Ret|=PH_AbsPosition(atof(argv[i]));
			if (Ret) break;
			do msleep(Phytron.msMotionPolling); while (PH_GetStatusAxes());
			PH_DbgGetPosition(P[i]);
		}
		PAUSE;
	}

	Ret|=PH_MoveToLimit(Phytron.MainDir);		// If no contactor, will stop after a timeout (18s I think. It's one of the parameters)
	do msleep(Phytron.msMotionPolling); while (PH_GetStatusAxes());

// Now with encoder
	// This works, but there seems to be no benefit at standard speeds
	Ret|=PH_SetParamValueF(19, 0.);				// Reset EL0P
	PH_SetParamValue(34, 1);					// Use incremental encoder
	for (i=1; i<argc; i++) {
		Ret|=PH_AbsPosition(atof(argv[i]));
		if (Ret) break;
		do msleep(Phytron.msMotionPolling); while (PH_GetStatusAxes());
		PH_DbgGetPosition(Q[i]);
		PAUSE;
	}
	double Qerr=0;
	double Perr=0;

	printf(BLD WHT "%s\n" NRM, TB_Phytron_Status());

	Ret|=PH_EnAxisPowerStage(0);	// Turn off

	// Print positionnning test results. The first argv should be 0 for referencing
	printf("\nParam:\t%d\t%ddeg\t%d\t%ddeg\t%d\t%ddeg\t%d\t%ddeg\t22/19", 19, 19, 20, 20, 21, 21, 22, 22);
	for (i=1; i<argc; i++) {
		double *p=P[i];
		printf("\nArg:\t\t%.2f", atof(argv[i]));
		printf("\nnoenc\t%.2f\t%.2f\t%.0f\t%.2f\t%.0f\t%.2f", 
			   p[19], P2D(p[19]), 
			   p[20], P2D(p[20]), 
			   p[21], P2D(p[21]));

		// Encoder
		double *q=Q[i];
		printf("\nenc\t%.2f\t%.2f\t%.0f\t%.2f\t%.0f\t%.2f\t%.0f\t%.1f\t%.1f", 
			   q[19], P2D(q[19]), 
			   q[20], P2D(q[20]), 
			   q[21], P2D(q[21]), 
			   q[22], P2D(q[22]), 
			   q[19]==0 ? 0 : q[22]/q[19]);	// This ratio seems to be near -333.6 ?!?
		printf("\ndif\t%.2f\t%.2f\t%.0f\t%.2f\t%.0f\t%.2f", 
			   p[19]-q[19], P2D(p[19]-q[19]), 
			   p[20]-q[20], P2D(p[20]-q[20]), 
			   p[21]-q[21], P2D(p[21]-q[21]));
		Qerr+=fabs(P2D(q[19])-atof(argv[i]));
		
		Perr+=fabs(P2D(p[19])-atof(argv[i]));
	}
	if (argc>1) {
		printf("\nnoenc average positionning error at StepRes=%d:\t%.2fdeg\n\n", StepRes, Perr/(argc-1));
		// Encoder
		printf("\nencod average positionning error at StepRes=%d:\t%.2fdeg\n\n", StepRes, Qerr/(argc-1));

		printf(BLD WHT "%s\n" NRM, TB_Phytron_Status());
	}


Skip:
	printf("\n================== Closing ==================\n");
	PH_CloseAll();
	if (Ret) printf(BLD RED "\n[Error condition %d present]\n" NRM, Ret);
	return Ret;
}
#endif



///////////////////////////////////////////////////////////////////////////////
/// HIFN	Init sequence for Phytron
/// HIRET	Any problem during the sequence will return a non-zero value
///////////////////////////////////////////////////////////////////////////////
int TB_Phytron_Init(void) {
	char Reply[MAX_REPLY];
	int Ret=0;
	
	if (PH_Init()) return errno;

	if ((Ret=PH_SendCommand(Reply, "IVR"))) return Ret;
	printf(BLD YEL "%s%s\n" NRM, Tabs, Reply);
	if ((Ret=PH_Reset())) return Ret;
	if ((Ret=PH_DoGetGeneralStatus())) return Ret;

	if ((Ret=PH_SetParamValue(45, Phytron.SpeedDiv))) return Ret;// Full step. Default was 1/6 ?
	if ((Ret=PH_SetParamValue(2, 1))) return Ret;				// Measuring unit of movement - Step
	if ((Ret=PH_SetParamValue(39, GearReduction))) return Ret;	// Encoder conversion factor - DOES NOT SEEM TO DO ANYTHING

	if ((Ret=PH_SetParamValue (34, Phytron.UseEncoder))) return Ret;

	// Check the effect of limits/timeout on initiator motion
	Ret|=PH_SetParamValue(23, D2P(90));				// Axis + limitation
	Ret|=PH_SetParamValue(24, D2P(0));				// Axis - limitation
	// It works but it doesn't make sense: it should be the opposite

	if ((Ret=PH_MoveToLimit(Phytron.MainDir))) return Ret;
	do msleep(Phytron.msMotionPolling); while (PH_GetStatusAxes());
	if ((Ret=PH_SetParamValueF(19, 0.))) return Ret;			// Reset EL0P
		
	printf(BLD GRN "================== PHYTRON MOTOR READY ==================\n" NRM);
	Enabled=1;
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Power off the Phytron motor, stop talking to the controller
///////////////////////////////////////////////////////////////////////////////
int TB_Phytron_Close(void) {
	PH_EnAxisPowerStage(0);	// Turn off
	PH_CloseAll();
	Enabled=0;
	printf(BLD YEL "================== PHYTRON MOTOR CLOSED ==================\n" NRM);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	General status report of Phytron
/// HIRET	Contains name of controller, system status and limits
///////////////////////////////////////////////////////////////////////////////
char* TB_Phytron_Status(void) {
	char Reply[256]="";
	int Ret, Status;
	static char String[1024]; String[0]='\0';
	if (!Enabled) return "Phytron controller not initialized or closed";

	if ((Ret=PH_SendCommand(Reply, "IVR"))) return "Phytron controller in error";
	sprintf(String, "%s\n", Reply);

	PH_SendCommand(Reply, "SB");
	sprintf(String+strlen(String), "SB: %s\n", PH_GetStatusBinaryMsg(Reply));

	PH_SendCommand(Reply, "ST");
	sprintf(String+strlen(String), "ST: %s\n", PH_GetSystemStatusMsg(Status = strtoul(Reply, NULL, 0x10)));

	PH_SendCommand(Reply, "SE");
	sprintf(String+strlen(String), "SE: %s\n", PH_GetSystemStatusExtendedMsg(Status = strtoul(Reply, NULL, 0x10)));

	sprintf(String+strlen(String), "Encoder: %s\n", PH_GetParamValue(34)?"ON":"OFF");
	sprintf(String+strlen(String), "M0P offset: +:%d, -:%d\n", PH_GetParamValue(11), PH_GetParamValue(12));
	sprintf(String+strlen(String), "Axis limit pos: +:%.1fdeg, -:%.1fdeg\n", P2D(PH_GetParamValue(23)), P2D(PH_GetParamValue(24)));

//	sprintf(String+strlen(String), "Hardware limits: -Inf..+Inf\n");	// TODO: check if there is a parameter for that
	sprintf(String+strlen(String), "Software limits: %.0f..%.0f", Phytron.OpMin, Phytron.OpMax);

	return String;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Move to absolute position
/// HIPAR	Angle/in degrees
///////////////////////////////////////////////////////////////////////////////
int TB_Phytron_MoveAbs(double Angle) {
	int Ret;
	if (!Enabled) { PrintErr("The system is not ready, call TB_Phytron_Init() before"); return 1; }
	if (Angle<Phytron.OpMin or Phytron.OpMax<Angle) { 
					PrintErr("Attempt to move to %.1f out of allowed range %.0f..%.0fdeg", 
							 Angle, Phytron.OpMin, Phytron.OpMax); return 1; }
	// TODO: sanity check, but not really necessary as we have full 360deg motion
	if ((Ret=PH_AbsPosition(Angle))) return Ret;
	while (PH_GetStatusAxes()) msleep(Phytron.msMotionPolling);
//	PH_DbgGetPosition(P[i]);
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Set the speed divisor
/// HIPAR	SD/1 for highest speed, 255 for lowest
///////////////////////////////////////////////////////////////////////////////
int TB_Phytron_SpeedDiv(int SD) {
	return PH_SetParamValue(45, Phytron.SpeedDiv = (SD<1 ? 1 : SD>256 ? 256 : SD));
}

///////////////////////////////////////////////////////////////////////////////
/// HIFN	Move the diode in the path of the beam
/// HIPAR	Do/0 to move the diode to the rest position, 1 to move it in the path of the beam
///////////////////////////////////////////////////////////////////////////////
extern int TB_Phytron_MoveInDiode(int Do) {
	return TB_Phytron_MoveAbs(Do ? Phytron.MoveInDiode_Pos : 0);
}

///////////////////////////////////////////////////////////////////////////////
char* TB_Phytron_LastErrMsg(void) {
	return LastErrMsg;
}
