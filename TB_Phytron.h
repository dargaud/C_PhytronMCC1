#ifndef __TB_PHYTRON_H
#define __TB_PHYTRON_H

// Some (like IP and Port) must be set before calling PH_Init()
typedef struct sPhytron {
	char IP[80];			// This is defined by the dhcp server configuration. See /etc/dhcp/dhcpd.conf file
	int  Port;				// Note that the Phytron opens several ports for various uses
	int msWait;				// Min delay between command and reply and new command. 50ms seems safe enough
	int msMotionPolling;	// Same as above, but higher value to limit the number of polling messages
	int SpeedDiv;			// Speed divisor (1-fastest to 256-slowest)
	double MoveInDiode_Pos;	// Precise angle where the diode is in the middle of the beam
	double OpMin, OpMax;	// Operational motion limits (in deg)
	int MainDir;			// +1 if the homing should go for the positive direction, -1 otherwise
	int UseEncoder;			// 1 to use encoder (if available). Must be set BEFORE init()
} tPhytron;
extern tPhytron Phytron;

// Low level commands
extern int PH_Init(void);
extern int PH_CloseAll(void);

extern int PH_SendCommand(char* Reply, const char* Command, ... )
						__attribute__ (( __format__( __printf__, 2, 3 ) ));

// Higher level
extern int PH_Reset(void);

extern char* PH_GetStatusBinaryMsg(char *Reply);
extern char* PH_GetSystemStatusMsg(int);
extern char* PH_GetSystemStatusExtendedMsg(int);

extern int PH_GetStatusAxes(void);
extern int PH_GetInitiatorsStatus(void);
extern int PH_Reset(void);
extern int PH_MoveToLimit(int Dir);
extern int PH_DoAct(void);
extern int PH_GetAxisCount(void);
extern int PH_GetSystemStatusAxes(void);
extern int PH_AxisStatusRequest(int val);
extern int PH_ResetAxis(void);
extern int PH_DoAxisStop(void);
extern int PH_EnAxisPowerStage(int val);
extern int PH_RelPosition(double val);	// WARNING: Does not enforce limits (see below)
extern int PH_AbsPosition(double val);	// WARNING: Does not enforce limits
extern int PH_FreeRunning(int val);		// WARNING: Does not enforce limits

extern int PH_SetParamValue(int param,int val);
extern int PH_GetParamValue(int param);
extern int PH_SetParamValueF(int Param, double val);
extern double PH_GetParamValueF(int Param);

extern double PH_DbgGetPosition(double *P);
extern double PH_GetPosition(void);

// Higher level still
extern int PH_DoGetGeneralStatus(void);

// For use by common control program
extern int TB_Phytron_Init(void);
extern int TB_Phytron_Close(void);
extern int TB_Phytron_MoveAbs(double Angle);	// Enforces operational limits
extern char* TB_Phytron_Status(void);
extern char* TB_Phytron_LastErrMsg(void);
extern int TB_Phytron_SpeedDiv(int SD);
extern int TB_Phytron_MoveInDiode(int Do);

#endif
