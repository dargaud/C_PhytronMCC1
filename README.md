# C_PhytronMCC1

C library and test programs to control a Phytron MCC-1 controller and associated motors.

More info here: https://www.gdargaud.net/Hack/Phytron.html
